import logging
import os

from tsbot import TS3Bot, TS3Server, TS3BotEvent

from dotenv import load_dotenv

load_dotenv()

server = TS3Server(
    server=os.getenv("SERVER", ""),
    port=os.getenv("PORT", 10011),
    user=os.getenv("CLIENT_NAME", ""),
    password=os.getenv("CLIENT_PASSWORD", ""),
)


def main():
    bot = TS3Bot(server_config=server, owner=os.getenv("OWNER"))

    @bot.command("console", "c")
    @bot.check_server_groups("(Officer)")
    def open_console(ctx, *_args, **_kwargs):
        bot.respond(ctx, msg="Console open", dm=True)

    @bot.on("permission_error")
    @bot.on("command_error")
    def reply_with_error(event: TS3BotEvent):
        bot.respond(event.ctx, event.msg)

    @bot.on("run")
    def move_to_lobby(_event, *_args, **_kwargs):
        bot.self.move(bot.channel.get_id("War Lobby"))
        logging.info("Running... Moved to Lobby")

    @bot.on("quit")
    def handle_quit(_event, *_args, **_kwargs):
        logging.info("Quiting")

    # noinspection PyUnresolvedReferences
    import logger

    bot.load_plugin("towns", "squad", "misc", "admin")
    bot.run()


if __name__ == '__main__':
    main()
